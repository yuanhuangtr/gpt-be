from flask import Flask, jsonify, request
import openai

from chatgpt.embedding import get_embedding
from chatgpt.init_openai import init_openai
from data_access.collections.common_knowledge import search_by_embedding
from data_access.init_db import init_db

init_db()
init_openai()

app = Flask(__name__)


@app.route("/", methods=['POST'])
def hello():
    req = request.get_json()
    text = req["text"]

    # find context

    # call chat gpt api
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": text},
        ],
    )

    return jsonify({'message': response.choices[0].message})


@app.route("/knowledge", methods=['POST'])
def knowledge_chat():
    req = request.get_json()
    text = req["text"]

    # find context

    # get the embedding for text
    embedding = get_embedding(text)
    contexts = search_by_embedding(embedding, 7)

    prompts = "Base on the given context to answer the question,\n"
    prompts = prompts + "if the context is not relevant to the question, answer me: Not_Found.\n"
    prompts = prompts + "Use the question's language to answer the question\n"
    prompts = prompts + "The context is: \n"
    for c in contexts:
        prompts = prompts + c.get("text") + "\n"

    prompts = prompts + "The question is: " + text + "\n"


    # call chat gpt api
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": prompts},
        ],
    )

    print(prompts)

    return jsonify({'message': response.choices[0].message})

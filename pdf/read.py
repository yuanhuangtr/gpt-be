import PyPDF2

from utils.token_size import num_tokens_from_string

# Open the pdf file in read-binary mode
with open('../docs/食品添加剂使用标准.pdf', 'rb') as pdf_file:
    # Creating a PdfFileReader object to read the pdf
    pdf_reader = PyPDF2.PdfFileReader(pdf_file)

    # Get the total number of pages in pdf
    total_pages = pdf_reader.numPages
    print(f'Total number of pages in pdf: {total_pages}')

    # Access the content of the first page
    page = pdf_reader.getPage(7)
    page_content = page.extractText()

    print(num_tokens_from_string(page_content, "gpt-3.5-turbo"))
    # Print the content of the first page
    print(f'First page content:\n{page_content}')


from scipy.spatial.distance import cdist


def distance_check(a, b):
    dist_euclidean = cdist([a], [b], 'euclidean')[0][0]
    return dist_euclidean

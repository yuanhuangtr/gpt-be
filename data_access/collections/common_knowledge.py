from typing import List

import pandas
from pymilvus import CollectionSchema, FieldSchema, DataType, Collection, utility

dim = 1536

knowledge_id = FieldSchema(
    name="id",
    dtype=DataType.INT64,
    is_primary=True,
    auto_id=True
)

# the pdf file, csv, excel, or url
source = FieldSchema(
    name="source",
    dtype=DataType.VARCHAR,
    max_length=100,
)

text = FieldSchema(
    name="text",
    dtype=DataType.VARCHAR,
    max_length=5000,
)

vector = FieldSchema(
    name="embedding",
    dtype=DataType.FLOAT_VECTOR,
    dim=dim
)

schema = CollectionSchema(
    fields=[knowledge_id, source, text, vector],
    description="Test knowledge search"
)

collection_name = "common_knowledge"


def get_collection() -> Collection:
    """
    if the collection existed return, if not create
    :return:
    """

    if utility.has_collection(collection_name):
        return Collection(collection_name)

    collection = Collection(
        name=collection_name,
        schema=schema,
        using='default',
    )

    # also create index
    index_params = {
        "metric_type": "L2",
        "index_type": "IVF_FLAT",
        "params": {"nlist": 1024}
    }

    collection.create_index(
        field_name="embedding",
        index_params=index_params
    )

    return collection


def drop_collection() -> None:
    utility.drop_collection(collection_name)


def insert_data(data: [List, pandas.DataFrame]):
    collection = get_collection()  # Get an existing collection.
    mr = collection.insert(data)
    collection.flush()
    # collection.release()
    return mr


def insert_single(s: str, t: str, embedding: List):
    res = search_by_embedding(embedding, 1)
    if len(res) == 1 and res[0]["text"] == t:
        return None
    return insert_data([[s], [t], [embedding]])


def delete_data(expr):
    collection = get_collection()  # Get an existing collection.
    collection.delete(expr)


def search_by_embedding(data, limit=3):
    collection = get_collection()
    collection.load()
    search_params = {
        "metric_type": "L2",
        "params": {"nprobe": limit}
    }
    results = collection.search(
        data=[data],
        anns_field="embedding",
        param=search_params,
        limit=limit, output_fields=["id", "source", "text"]
    )

    arr = []

    for hits in results:
        for hit in hits:
            arr.append(
                {"id": hit.entity.get('id'), "text": hit.entity.get('text'), "source": hit.entity.get("source")})

    return arr

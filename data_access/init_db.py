import os
from pymilvus import (
    connections,
)

PARAMETER = {
    'server': 'localhost',
    'port': '19530',
    'user': "",
    'pwd': ""
}


def init_db() -> None:
    env_parameter = {
        'server': os.getenv("MILVUS_SERVER"),
        'port': os.getenv("MILVUS_PORT"),
        'user': os.getenv("MILVUS_USERNAME"),
        'pwd': os.getenv("MILVUS_PASSWORD")
    }

    filter_env = {k: v for k, v in env_parameter.items() if v is not None}

    PARAMETER.update(filter_env)

    connections.connect("default", host=PARAMETER["server"], port=PARAMETER["port"], user=PARAMETER["user"],
                        password=PARAMETER["pwd"])

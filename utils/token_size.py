import tiktoken
'''
Encoding name	OpenAI models
cl100k_base	gpt-4, gpt-3.5-turbo, text-embedding-ada-002
p50k_base	Codex models, text-davinci-002, text-davinci-003
r50k_base (or gpt2)	GPT-3 models like davinci
'''


# encoding = tiktoken.get_encoding("cl100k_base")
def num_tokens_from_string(string: str, model_name="gpt-3.5-turbo") -> int:
    encoding = tiktoken.encoding_for_model(model_name)
    encoding.encode(string)
    num_tokens = len(encoding.encode(string))
    return num_tokens


num = num_tokens_from_string("tiktoken is great!", "gpt-3.5-turbo")
print(num)


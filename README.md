# gpt-BE

## environment


### milvus

start db:

```commandline
sudo docker-compose up -d
```

stop db:

```commandline
sudo docker-compose down
```

To delete data after stopping Milvus, run:

```commandline
sudo rm -rf volumes
```

### python packages

create venv :

```commandline
python -m venv venv
```

activate venv:

```commandline
source venv/bin/activate
```
install packages:

```commandline
pip install -r requirements.txt
```

### run test file

```commandline
python ./data_access/hello_milvus.py 
```

if this file could run correctly, means the db is working.


### run flask

```commandline
flask run
```


## save current env to requirements

```commandline
pip freeze > requirements.txt
```

import PyPDF2

from chatgpt.embedding import get_embedding
from data_access.collections.common_knowledge import insert_single
from utils.token_size import num_tokens_from_string
import os

LIMIT = 500


def import_pdf(path: str):
    with open(path, 'rb') as pdf_file:
        pdf_reader = PyPDF2.PdfFileReader(pdf_file)
        total_pages = pdf_reader.numPages
        for i in range(total_pages):
            page = pdf_reader.getPage(i)
            page_content = page.extractText()
            import_content_to_db(page_content, os.path.basename(path))
    print("the file path:" + path + " is imported")


def import_content_to_db(content: str, file: str):
    arr = []
    token_size = 0
    lines = content.split('\n')
    for idx in range(len(lines)):
        line = lines[idx]
        num = num_tokens_from_string(line)
        if token_size + num > LIMIT:
            batch = '\n'.join(arr)
            embedding = get_embedding(batch)
            insert_single(file, batch, embedding)
            arr = [line]
            token_size = num
        elif len(lines) == (idx + 1):
            arr.append(line)
            batch = '\n'.join(arr)
            embedding = get_embedding(batch)
            insert_single(file, batch, embedding)
            arr = []
            token_size = 0
        else:
            arr.append(line)
            token_size += num


def import_pdf_files(path: str):
    """
    import the pdf by path
    :param path: path of the folder
    :return:
    """
    pdf_files = [os.path.join(path, file) for file in os.listdir(path) if file.endswith('.pdf')]
    for file in pdf_files:
        import_pdf(file)

from dotenv import load_dotenv
import os
import openai

load_dotenv()


def init_openai():
    openai.api_key = os.getenv("OPENAI_API_KEY")
    openai.organization = os.getenv("OPENAI_ORGANIZATION")

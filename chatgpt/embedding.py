from typing import List

import openai


def get_embedding(text: str)->List:
    response = openai.Embedding.create(model="text-embedding-ada-002",
                                       input=text)

    return response.data[0].embedding

import openai

from business.import_pdf import import_content_to_db, import_pdf, import_pdf_files
from chatgpt.embedding import get_embedding
from chatgpt.init_openai import init_openai
from data_access.collections.common_knowledge import insert_data, search_by_embedding, drop_collection, insert_single
from data_access.init_db import init_db
import os

def init() -> None:
    init_db()
    init_openai()


    # import all pdf files
    folder_path = "./docs"
    import_pdf_files(folder_path)

    # drop_collection()

    # import_pdf("./docs/食品添加剂使用标准.pdf")

    # str1 = "本标准规定了食品添加剂的使用原则、允许使用的食品添加剂品种、使用范围及最大使用量或残留量。"
    # embedding1 = get_embedding(str1)
    #
    # insert_single(embedding1, str1)

    # str2 = "协定税率"
    # embedding2 = get_embedding(str2)
    # #
    # res = search_by_embedding(embedding2, 10)
    # for obj in res:
    #     print(obj["text"])
    #


    # print(response.choices[0].text)


init()
